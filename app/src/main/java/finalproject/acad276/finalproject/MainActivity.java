package finalproject.acad276.finalproject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseObject;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseUser;

public class MainActivity extends Activity {

    Button signUpButton;
    Button loginButton;
    MediaPlayer mySound;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_main);
        StartAnimations();}

//    mySound.start();

        private void StartAnimations() {
//            Animation anim = AnimationUtils.loadAnimation(this, R.anim.rotate);
//            anim.reset();
//            LinearLayout l = (LinearLayout) findViewById(R.id.hello);
//            l.clearAnimation();
//            l.startAnimation(anim);



            mySound = MediaPlayer.create(this, R.raw.pen); //sounds for onclick
        signUpButton = (Button) findViewById(R.id.signUpButton);
        loginButton = (Button) findViewById(R.id.loginButton);

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), SignUp.class);
                mySound.start();
                startActivity(i); //sends users to sign up
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Login.class);
                mySound.start();
                startActivity(i); //sends user to login
            }
        });
    }

}
