package finalproject.acad276.finalproject;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import static com.parse.ParseUser.getCurrentUser;


@ParseClassName(Review.CLASS_NAME)
public class Review extends ParseObject{
//we have to create a parse class so that this works
    public static final String CLASS_NAME = "Review";
    private String reviewTitleText;
    private Double reviewRating;
    private String reviewUsername;
    private String userReviewText;
    private Float reviewAverage;
//    ParseObject hi;


    public void setReviewTitleText(String reviewTitleText) {
        this.reviewTitleText = reviewTitleText;
    }
    public String getReviewTitleText() {

        return reviewTitleText;
    }

    public void setRating(Double reviewRating) {
        this.reviewRating = reviewRating;
    }
    public Double getRating() {
        return reviewRating;
    }


//    public ParseUser setReviewUsername(ParseUser user) {
//        return ParseUser.getCurrentUser();
//    }

    public void setReviewUsername(String username) {
        this.reviewUsername = username;
    }

    public String getReviewUsername() {
        return this.reviewUsername;
    }

    public void setUserReviewText(String userReviewText) {
        this.userReviewText = userReviewText;
    }
    public String getUserReviewText() {
        return userReviewText;
    }

    public void setAverage(Float userReviewText) {

        this.reviewAverage = userReviewText;
    }
    public Float getAverage() {

        return reviewAverage;
    }
//all of our data types are important to get reviews, names, and ratings form Parse
        public void saveData() throws ParseException{
//we have to throw exception for try/catch and then put the data into Parse/elsewheere and save it
        put ("Title", reviewTitleText);
        put("Rating", reviewRating);
        put("Username", reviewUsername);
        put("Review", userReviewText);
//        put("Averages", reviewAverage);
        save();
    }
}

