package finalproject.acad276.finalproject;

/**
 * Created by sereneboachie on 12/7/15.
 */
public class Course {
//this is our class for our accessors and mutators for the course listview adaptor and activity

    private float ratingStar;
    private String num;
    private String name;

    public Course(float ratingStar, String num, String name) {
        this.ratingStar = ratingStar;
        this.num = num;
        this.name = name;
    }

    public float getRatingStar() {
        return ratingStar;
    }

    public void setRatingStar(float ratingStar) {
        this.ratingStar = ratingStar;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
