package finalproject.acad276.finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class NoCoursesYetActivity extends AppCompatActivity {
//we use this page to tell users that there are no courses available for review yer
    Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_courses_yet);
        setTitle("NO COURSES YET");

        backButton = (Button) findViewById(R.id.backButton);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MajorListActivity.class);
                startActivity(intent);
            }
        }); //send users back to majors

    }
}
