package finalproject.acad276.finalproject;

/**
 * Created by sereneboachie on 12/7/15.
 */

import android.app.Activity;
import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.graphics.Movie;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

class CourseListViewAdaptor extends ArrayAdapter<Course> {

    private AppCompatActivity courseActivity;
    private ArrayList<Course> courseList;
    private ArrayList<Course> orig; //create two lists for the search function

    public CourseListViewAdaptor(AppCompatActivity context, int resource, ArrayList<Course> objects) {
        super(context, resource, objects);
        this.courseActivity = context;
        this.courseList = objects;
    }

    @Override
    public Course getItem(int position) {
        return courseList.get(position);
    } //prepare the list

    @Override
    public long getItemId(int position) {
        return position;
    }//get results


    public class CourseHolder {
        RatingBar ratingBar;
        TextView courseNum;
        TextView courseName;

    } //create holder for main functionality




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CourseHolder cholder;
//        LayoutInflater cinflater = (LayoutInflater) courseActivity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = LayoutInflater.from(courseActivity).inflate(R.layout.course_item_listview, parent, false);
            cholder = new CourseHolder();
            cholder.ratingBar = (RatingBar) convertView.findViewById(R.id.courseRatingBar);
            cholder.courseNum = (TextView) convertView.findViewById(R.id.courseNumText);
            cholder.courseName = (TextView) convertView.findViewById(R.id.courseNameText);
            convertView.setTag(cholder); //set up the view through the holder
        }
        else {
            cholder = (CourseHolder) convertView.getTag();
            cholder.ratingBar.getTag(position);
        }

//        cholder.ratingBar.setOnRatingBarChangeListener(onRatingChangedListener(cholder, position));

        cholder.ratingBar.setTag(position);
        cholder.ratingBar.setRating(courseList.get(position).getRatingStar());
        cholder.courseNum.setText(courseList.get(position).getNum());
        cholder.courseName.setText(courseList.get(position).getName());

        return convertView; //set positions of everything for proper formatting and drawing of results
    }

    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<Course> results = new ArrayList<>();
                if (orig == null)
                    orig = courseList;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final Course g : orig) {
                            if (g.getName().toLowerCase()
                                    .contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn; //allows for the users query to filter through what the listview has available
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                courseList = (ArrayList<Course>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void notifyDataSetChanged() {

        super.notifyDataSetChanged();
    }//tell Android something is different

    @Override
    public int getCount() {
        return courseList.size();
    }}//get length of array



