package finalproject.acad276.finalproject;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseObject;
import com.parse.ParseUser;

//this class is used to connect to parse so that we can use our app effectively with their database

/**
 * Created by sereneboachie on 11/23/15.
 */
public class RateMyMajorApplication extends Application {
    public RateMyMajorApplication() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Enable Local Datastore.
//        Parse.enableLocalDatastore(this);
        ParseObject.registerSubclass(Review.class);
        ParseObject.registerSubclass(Rating.class);
        Parse.initialize(this, "nxRwJLuy1kRLNQzGwLm7WPArODcJKXa6DSxVZNkk", "G5R0SARgQjpSfvEte3mUCRkvkClnEVRa6t9ITstf");
        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);
    }
}
