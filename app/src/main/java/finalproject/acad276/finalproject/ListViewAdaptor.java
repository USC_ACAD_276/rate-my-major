package finalproject.acad276.finalproject;

/**
 * Created by sereneboachie on 12/7/15.
 */

import android.app.Activity;
import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.graphics.Movie;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;
//import java.util.logging.Filter;

public class ListViewAdaptor extends ArrayAdapter<Major> implements Filterable { //filter for search query

    private AppCompatActivity activity;
//    private List<Major> majorList;
   private ArrayList<Major> majorList;
    private ArrayList<Major> orig;
    public ListViewAdaptor(AppCompatActivity context, int resource, ArrayList<Major> objects) {
        super(context, resource, objects);
        this.activity = context;
        this.majorList = objects;
    }

    @Override
    public Major getItem(int position) {
        return majorList.get(position);
    } //position in array list

    @Override
    public long getItemId(int position) {
        return position;
    }//return it as an int


    public class MajorHolder {
    RatingBar ratingBar;
    TextView majorName;
// set up holder for usage in creating list view
}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MajorHolder holder;
//        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.item_listview, parent, false);
            holder = new MajorHolder();
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.majorRatingBar);
            holder.majorName = (TextView) convertView.findViewById(R.id.majorNameText);
            convertView.setTag(holder); //this draws ids to then be used for positioning

        } else {
            holder = (MajorHolder) convertView.getTag();
            holder.ratingBar.getTag(position);
        }

//        holder.ratingBar.setOnRatingBarChangeListener(onRatingChangedListener(holder, position));

        holder.ratingBar.setTag(position);
        holder.ratingBar.setRating(majorList.get(position).getRatingStar());
        holder.majorName.setText(majorList.get(position).getName()); //draw results for viewer to see
        String major = holder.majorName.getText().toString();
        System.out.println(holder.majorName);
        return convertView;
    }

    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<Major> results = new ArrayList<>();
                if (orig == null)
                    orig = majorList;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final Major g : orig) {
                            if (g.getName().toLowerCase()
                                    .contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn; //filters results from users query
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                majorList = (ArrayList<Major>) results.values;
                notifyDataSetChanged(); //refresh with new data
            }
        };
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

@Override
public int getCount() {

    return majorList.size();

}} // get size of arraylist

