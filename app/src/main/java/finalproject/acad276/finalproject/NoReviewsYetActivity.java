package finalproject.acad276.finalproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class NoReviewsYetActivity extends Activity {
//if a class has no review, users see this activity
    Button beTheFirstButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_reviews_yet);
        setTitle("NO REVIEWS YET");

        beTheFirstButton = (Button) findViewById(R.id.beTheFirstButton);

        beTheFirstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SubmitReview.class);
                startActivity(intent);
            }
        });//allow them to submit a review and be the first!
    }
}
