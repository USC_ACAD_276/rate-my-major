package finalproject.acad276.finalproject;

import android.content.Intent;
import android.media.Rating;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;

import java.util.List;
import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import com.parse.ParseFile;

public class ReviewPageActivity extends AppCompatActivity {

    Button leaveReviewButton;
    EditText reviewTitle;
    RatingBar userRating;
    ParseUser username;
    EditText userReview;
    RatingBar courseRating;

    ListView reviewList;
    List<ParseObject> ob;
    ProgressDialog mProgressDialog;
    ReviewAdaptor reviewArrayAdapter;
    private List<Review> reviewslist;
    float avgCourseRating;
    float averageCourseRating;

//    private SwipeRefreshLayout swipeRefreshLayout;
//    ParseObject userInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_page);
        setTitle("REVIEWS");
        leaveReviewButton = (Button) findViewById(R.id.leaveReviewButton);
        courseRating = (RatingBar) findViewById(R.id.courseRating);

        leaveReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SubmitReview.class);
                startActivity(intent); //allow users to actually submit and execute review
            }
        });
//        ParseObject.registerSubclass(Review.class);
        new RemoteDataTask().execute();
    }


//     RemoteDataTask AsyncTask
    private class RemoteDataTask extends  AsyncTask<Void, Void, Void> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            // Create a progressdialog
//            mProgressDialog = new ProgressDialog(ReviewPageActivity.this);
//            // Set progressdialog title
//            mProgressDialog.setTitle("Parse.com Custom ListView Tutorial");
//            // Set progressdialog message
//            mProgressDialog.setMessage("Loading...");
//            mProgressDialog.setIndeterminate(false);
//            // Show progressdialog
//            mProgressDialog.show();
//        }

        @Override
        protected Void doInBackground(Void... params) {
            reviewslist = new ArrayList<Review>();
            ParseQuery<ParseObject> query = ParseQuery.getQuery(Review.CLASS_NAME);
            query.orderByDescending("createdAt");
            Double theRating = 0.0;
            double sum = 0;
//here we need to access parse to send viable data over
            try {

                ob = query.find();
                double runningTotal = 0;
                int reviewCount = 0;
                for (ParseObject review : ob) {

                    Review reviews = new Review(); //new empty object
                    Double rating = new Double(review.get("Rating").toString());


                    reviews.setReviewTitleText((String) review.get("Title"));
                    reviews.setRating(rating);
                    reviews.setReviewUsername((String) review.get("username"));
                    reviews.setUserReviewText((String) review.get("Review"));


                    reviewslist.add(reviews); //we construct our reviews above then add them to our arraylist here


                    runningTotal = runningTotal + rating.doubleValue();
                    reviewCount++; //data verification)



                }
                Review reviews = new Review();
                ParseObject help = ParseObject.create("ClassAverages");
                System.out.println("Average: " + runningTotal / (float)reviewCount);
                averageCourseRating = (float) runningTotal / (float) reviewCount;
                float wholeNumber = (float)Math.round((averageCourseRating * 10));
                avgCourseRating = wholeNumber/10.0f;
                Float avg = avgCourseRating;
                reviews.setAverage(avgCourseRating);
                System.out.println("Huh = " + reviews.getAverage());
                help.put("Averages", reviews.getAverage());
                help.saveInBackground(); //we save in background our result so that we can then pull it later to retrieve the stars


            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            } //in case it fails

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            courseRating.setRating((float) avgCourseRating);

            reviewList = (ListView) findViewById(R.id.reviewListView);

            reviewArrayAdapter = new ReviewAdaptor(ReviewPageActivity.this,
                    reviewslist);

            reviewList.setAdapter(reviewArrayAdapter);

            final Review reviews = new Review();
        } //cast it as a float and retreieve it so that when we execute we communicate and utilize adaptor







    }}

