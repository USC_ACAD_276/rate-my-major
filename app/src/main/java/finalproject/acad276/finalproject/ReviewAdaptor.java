package finalproject.acad276.finalproject;

/**
 * Created by sereneboachie on 12/8/15.
 */

import android.app.Activity;
import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.content.Intent;
import android.graphics.Movie;
import android.media.Rating;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;


public class ReviewAdaptor extends BaseAdapter {

    // Declare Variables
    Context context;
    LayoutInflater inflater;
//    ImageLoader imageLoader;
    private List<Review> reviewslist = null;
    private ArrayList<Review> arraylist;

    public ReviewAdaptor(Context context,
                           List<Review> reviewslist) {
        this.context = context;
        this.reviewslist = reviewslist;
        inflater = LayoutInflater.from(context);
        this.arraylist = new ArrayList<Review>();
        this.arraylist.addAll(reviewslist);
//        imageLoader = new ImageLoader(context);
    }

    public class ViewHolder {
        TextView reviewTitleText;
        RatingBar reviewRating;
        TextView reviewText;
        TextView reviewUser;
//        ImageView flag;
    }//initialize what exactly we will be using in our page

    @Override
    public int getCount() {
        return reviewslist.size();
    }

    @Override
    public Object getItem(int position) {
        return reviewslist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    } //need this to construct page

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.review, null);
            // Locate the TextViews in listview_item.xml
            holder.reviewTitleText = (TextView) view.findViewById(R.id.reviewTitleText);
            holder.reviewRating = (RatingBar) view.findViewById(R.id.reviewRating);
//            holder.reviewUser = (TextView) view.findViewById(R.id.reviewUsername);
            holder.reviewText = (TextView) view.findViewById(R.id.userReviewText);

            view.setTag(holder); //use holder to draw content
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.reviewTitleText.setText(reviewslist.get(position).getReviewTitleText());
        System.out.print("type" + reviewslist.get(position).getRating());
        Float woohoo = (reviewslist.get(position).getRating()).floatValue();
        holder.reviewRating.setRating(woohoo);
        holder.reviewText.setText(reviewslist.get(position).getUserReviewText());
//        holder.reviewUser.setText(reviewslist.get(position).getReviewUsername());
        //in this adaptor, we are communicating with the app and parse to construct our reviest list and to retrieve ratings

        return view;
    }

}