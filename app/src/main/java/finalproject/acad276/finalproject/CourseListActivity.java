package finalproject.acad276.finalproject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.bluetooth.BluetoothClass;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;

import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class CourseListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    //extend and enable search capabilities

    private ListView courseListView;
    private ArrayAdapter<Course> courseAdapter;
    private ArrayList<Course> courseArrayList;
    private SearchView mSearchView;
    List<ParseObject> ob;
    RatingBar majorRating;
    float avgMajorRating;
    double average;
    TextView className;
//    ParseObject averages = ParseObject.create("ClassAverages");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_course);
        setTitle("Iovine & Young Academy"); //set title of top to the selected major
        mSearchView=(SearchView) findViewById(R.id.courseSearchView);
        courseListView = (ListView)findViewById(R.id.courseListView);
        majorRating = (RatingBar) findViewById(R.id.majorRating);
        setLisData(); //create data class
        courseAdapter = new CourseListViewAdaptor(this, R.layout.course_item_listview, courseArrayList);
        courseListView.setAdapter(courseAdapter);
        courseListView.setTextFilterEnabled(true);
        setupSearchView(); //create search abilities
        courseListView.setOnItemClickListener(onItemClickListener());



        courseListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                className = (TextView) view.findViewById(R.id.courseNameText);

                if(className.getText().toString().equalsIgnoreCase("Innovator's Forum")) {
                    Intent intent = new Intent(getApplicationContext(), ReviewPageActivity.class);
                    startActivity(intent);
                }
                else{
                    Intent intent = new Intent(getApplicationContext(), NoReviewsYetActivity.class);
                    startActivity(intent);
                } // these intents will allow users to see reviews for selected courses
            }
        });
    }

    private void setupSearchView()
    {
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(true);
        mSearchView.setQueryHint("Search Class Name, Not Num Please");
    } //this sets up search function

    @Override
    public boolean onQueryTextChange(String newText)
    {

        if (TextUtils.isEmpty(newText)) {
            courseListView.clearTextFilter();
        } else {
            courseListView.setFilterText(newText);
        }
        return true;
    } //this makes it so when the user enters the search space it clears

    @Override
    public boolean onQueryTextSubmit(String query)
    {
        return false;
    } //queries what users type


    private AdapterView.OnItemClickListener onItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                final Dialog dialog = new Dialog(MajorListActivity.this);
//                dialog.setContentView(R.layout.layout_dialog);
//                dialog.setTitle("Major details");
//
//                TextView name = (TextView) dialog.findViewById(R.id.major_name);
//                TextView starRate = (TextView) dialog.findViewById(R.id.rate);
//
//                Major major = (Major) parent.getAdapter().getItem(position);
//                name.setText("Major name: " + major.getName());
//                starRate.setText("Your rate: " + major.getRatingStar());
//
//                dialog.show();
            }
        };
    }



private void setLisData(){

    ParseQuery<ParseObject> query = ParseQuery.getQuery("ClassAverages");
    query.orderByDescending("createdAt"); //access parse for stars
//    query.whereEqualTo("Averages");
//    query.whereEqualTo("playerEmail", "dstemkoski@example.com");
    query.getFirstInBackground(new GetCallback<ParseObject>() {
        public void done(ParseObject object, ParseException e) {
            if (object == null) {
                Log.d("score", "The getFirst request failed.");
            } else {
                Log.d("score", "Retrieved the object.");
                average = object.getDouble("Averages");
                System.out.println("Averages=" + average);
                float myaverage = (float) average;
                Course hi = new Course(0.0f, "hey", "hi");
                hi.setRatingStar(myaverage);
                majorRating.setRating((float) average); //cast it as float according to parse
                courseArrayList.add(new Course((float) average, "ACAD 145", "Innovator's Forum")); //put in average for stars
                courseArrayList.add(new Course(0, "ACAD 175", "Innovator's Roundtable"));
                courseArrayList.add(new Course(0, "ACAD 176", "Rapid Visualization"));
                courseArrayList.add(new Course(0, "ACAD 177", "Digital Toolbox for Design"));
                courseArrayList.add(new Course(0, "ACAD 178", "Digital Toolbox: Motion Graphics"));
                courseArrayList.add(new Course(0, "ACAD 180", "Digital Toolbox: Sound & Audio"));
                courseArrayList.add(new Course(0, "ACAD 181", "Disruptive Innovation"));
                courseAdapter.notifyDataSetChanged();


            }
//            System.out.println("Averages=" + average);
        }

    });



    courseArrayList = new ArrayList<>();



    }
}
