package finalproject.acad276.finalproject;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;


public class SignUp extends AppCompatActivity {

    Button loginbutton, createAccountButton;
    String usernametext, passwordtext, emailtext;
    EditText password, username, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        setTitle("SIGN UP"); // sign up sheet create variables and access them through widgets

        //Declare variables
        username = (EditText) findViewById(R.id.nameText);
        email = (EditText) findViewById(R.id.emailText);
//        major = (EditText) findViewById(R.id.majorText);
//        gradClass = (EditText) findViewById(R.id.gradText);
        password = (EditText) findViewById(R.id.passwordText);

        Log.d("Just a test", "this works");

        createAccountButton = (Button) findViewById(R.id.createAccountButton);

        //Create account for user.
        createAccountButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                usernametext = username.getText().toString();
                passwordtext = password.getText().toString();
                emailtext = email.getText().toString();
                //get text for when button is pressed



                if (usernametext.equals("") && passwordtext.equals("") && emailtext.equals("")) {
                    Toast.makeText(getApplicationContext(), "Please complete the sign up form dude!", Toast.LENGTH_LONG).show();
                    //in case users forget the most important fields, verify here

                } else {
                    ParseUser user = new ParseUser();
                    user.setUsername(usernametext);
                    user.setPassword(passwordtext);
                    user.setEmail(emailtext);

                    user.signUpInBackground(new SignUpCallback() {
                        public void done(ParseException e) {
                            if (e == null) {
                                Intent intent = new Intent(
                                        getApplicationContext(),
                                        MajorListActivity.class);
                                Toast.makeText(getApplicationContext(), "Successful sign up.", Toast.LENGTH_LONG).show();
                                startActivity(intent); //if parse works
                            } else {
                                Log.e("SOMETHING IS WRONG", e.getLocalizedMessage());
                                Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                            } //If parse isn't working, this will read out
                        }
                    });
                }


            }
        });
    }}