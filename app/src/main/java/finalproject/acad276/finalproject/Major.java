package finalproject.acad276.finalproject;

/**
 * Created by sereneboachie on 12/7/15.
 */
public class Major {
//this is our accessors and mutators for the major activity and adaptor
    private float ratingStar;
    private String name;



    public Major(float ratingStar, String name) {
        this.ratingStar = ratingStar;
        this.name = name;
    }



    public float getRatingStar() {
        return ratingStar;
    }

    public void setRatingStar(float ratingStar) {
        this.ratingStar = ratingStar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
