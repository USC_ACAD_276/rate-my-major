package finalproject.acad276.finalproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class Login extends AppCompatActivity {
    Button loginbutton;
    String usernametext;
    String passwordtext;
    EditText password;
    EditText username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("LOG IN");

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password); //get what the user puts in

        loginbutton = (Button) findViewById(R.id.buttonLogIn); //respond to user login

        //When user clicks the log in button, it takes your input through Parse.
        loginbutton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0){
                usernametext = username.getText().toString();
                passwordtext = password.getText().toString(); //gets text to then verify with Parse

                ParseUser.logInInBackground(usernametext, passwordtext,
                        new LogInCallback() {
                            public void done(ParseUser user, ParseException e){
                                if (user!=null) {

                                    ParseObject userInfo = new ParseObject("UserInfo");
                                    userInfo.put("username", usernametext);
                                    userInfo.put("password", passwordtext);
                                    userInfo.saveInBackground(); // send to parse and see if they match, save that in background

                                    //Direct user to page with list of majors.
                                    Intent intent = new Intent(
                                            Login.this,
                                            MajorListActivity.class);
                                    startActivity(intent);
                                    Toast.makeText(getApplicationContext(),
                                            "Successfully logged in",
                                            Toast.LENGTH_LONG).show();
                                    finish(); //if they match up, you will be logged in and taken inside the app
                                }
                                else {
                                    Toast.makeText(getApplicationContext(), "Invalid information", Toast.LENGTH_LONG).show();
                                }//data verification
                            }});
                        }

            });}}








