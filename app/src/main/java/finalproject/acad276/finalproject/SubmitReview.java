package finalproject.acad276.finalproject;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.parse.ParseUser;

import java.util.List;

public class SubmitReview extends AppCompatActivity {

    Button submitButton;
    EditText reviewTitle;
    RatingBar userRating;
    ParseUser username;
    EditText userReview;
    ListView reviewList;
    MediaPlayer mySound;

    private List<Review> reviewslist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_review);
        setTitle("SUBMIT A REVIEW"); //this page allows users to actually submit the review they created
        mySound = MediaPlayer.create(this, R.raw.pen);
        submitButton = (Button) findViewById(R.id.submitButton);
        reviewTitle = (EditText) findViewById(R.id.reviewTitleInput);
        userRating = (RatingBar) findViewById(R.id.ratingInput);
        userReview = (EditText) findViewById(R.id.reviewInput);
        reviewList = (ListView) findViewById(R.id.reviewListView);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = reviewTitle.getText().toString();
                double rating = userRating.getRating();
//                String user = username.getUsername();
//                String user = userInfo.getString("username");
                String reviewText = userReview.getText().toString();
                //gets text as a string to be stored in parse


                //make sure that the user has entered data in the form
                if (title.length() == 0 || reviewText.length() == 0 || rating == 0) {
                    Toast.makeText(getApplicationContext(), "Please input a rating", Toast.LENGTH_SHORT).show();
                    //data verification
                } else { //user entered something
//                    ParseObject review = new Review();
                    final Review reviews = new Review(); //new empty object
//                    System.out.println("Huh = " + reviews.getAverage());
//                    System.out.println("average = " + reviewAverage);
                            reviews.setReviewTitleText(title);
                    reviews.setRating(rating);
//                    reviews.getUsername();
                    ParseUser.getCurrentUser();
                    reviews.setReviewUsername(ParseUser.getCurrentUser().getString("username"));
                    reviews.setUserReviewText(reviewText); //gets current user for our records on Parse
//                    reviews.setAverage(reviewAverage);

//                    Toast.makeText(getApplicationContext(), "FUCK BITCHES YEAH!", Toast.LENGTH_SHORT).show();
                    //too remember our frustration with things actually working
                    System.out.println("PRE ADDING A REVIEW");
//                    ReviewPageActivity reviews = new ReviewPageActivity();
//                    reviewslist.add(review);

                    try {
                        reviews.saveData();
                        mySound.start();
                                            Intent intent = new Intent(
                            SubmitReview.this,
                            CourseListActivity.class);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(),
                            "Thank you for your review!",
                            Toast.LENGTH_LONG).show();
                        //congratulate them on successful review and send them back users can then see their review
//
//                    }
//                    catch (ParseException e) {
//                        e.printStackTrace();
                    }
                    catch (com.parse.ParseException e) {
                        e.printStackTrace(); //go!
                    }

                }
            }
        });

    };
}
