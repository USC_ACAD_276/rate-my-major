package finalproject.acad276.finalproject;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by sereneboachie on 12/10/15.
 */
@ParseClassName(Rating.CLASS_NAME)
public class Rating extends ParseObject {
    public static final String CLASS_NAME = "ClassAverages";
    private Double courseRating;

    public void setCourseRating(Double reviewRating) {
        this.courseRating = reviewRating;
    }
    public Double getCourseRating() {
        return courseRating;
    }
}
//this small class allows us to utilize parse by connecting data with setters and getters