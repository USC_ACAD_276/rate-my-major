package finalproject.acad276.finalproject;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class splash_dash extends Activity {

    private static int SPLASH_TIME_OUT = 3000;
    ImageView rotateLogo;
    MediaPlayer mySound; //opening sound, length of page, and animation

   @Override
   protected void onCreate(Bundle savedInstanceState) {
     super.onCreate(savedInstanceState);
      setContentView(R.layout.splash_dash);
       mySound = MediaPlayer.create(this, R.raw.openingsound);
       mySound.start(); //start sound on load multiple locations in case of failure

       rotateLogo = (ImageView) findViewById(R.id.splashLogo);
       final Animation animRotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
       rotateLogo.startAnimation(animRotate); //rotate the logo
       mySound.start();

      new Handler().postDelayed(new Runnable() {



              @Override
      public void run() {
                  mySound.start();

                  Intent i = new Intent(splash_dash.this, MainActivity.class);
                  startActivity(i); //go to main log on / sign up page
              }
}, SPLASH_TIME_OUT);

}

}

