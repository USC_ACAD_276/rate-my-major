package finalproject.acad276.finalproject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.SearchManager;
import android.bluetooth.BluetoothClass;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.media.MediaPlayer;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import android.widget.SearchView;

import java.util.ArrayList;

public class MajorListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private ListView listView;
    private ArrayAdapter<Major> adapter;
    private ArrayList<Major> arrayList;
    private SearchView mSearchView;
    double average;
    TextView majorName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_major);

        setTitle("MAJORS");
        mSearchView=(SearchView) findViewById(R.id.majorSearchView);
        listView = (ListView) findViewById(R.id.majorListView);
        setListData();
        adapter = new ListViewAdaptor(this, R.layout.item_listview, arrayList);
        listView.setAdapter(adapter);

        listView.setTextFilterEnabled(true);
        setupSearchView();
// very similar to the course page, enable query and draw results from adaptor


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                majorName = (TextView) view.findViewById(R.id.majorNameText);
//                System.out.println("name = " + majorName.getText().toString());


                if(majorName.getText().toString().equalsIgnoreCase("Iovine & Young Academy")) {
                    Intent intent = new Intent(getApplicationContext(), CourseListActivity.class);
                    startActivity(intent);
                } // if you click a certain major, you will be taken to its courses
                else{
                    Intent intent = new Intent(getApplicationContext(), NoCoursesYetActivity.class);
                    startActivity(intent);
                }//if there's no courses yet, when you click a major you will be taken here
            }
        });
    };

    private void setupSearchView()
    {
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(true);
        mSearchView.setQueryHint("Search Here");
    } //allow for user query

    @Override
    public boolean onQueryTextChange(String newText)
    {

        if (TextUtils.isEmpty(newText)) {
            listView.clearTextFilter();
        } else {
            listView.setFilterText(newText);
        }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query)
    {
        return false;
    } //same as before, allow for the text to empty and then allow for text submit






    private void setListData() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("ClassAverages");
        query.orderByDescending("createdAt");
//    query.whereEqualTo("Averages");
//    query.whereEqualTo("playerEmail", "dstemkoski@example.com");

        //exact same as course activity, we need to pull the average from parse in order to update the amount of stars

        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("score", "The getFirst request failed.");
                } else {
                    Log.d("score", "Retrieved the object.");
                    average = object.getDouble("Averages");
                    System.out.println("Averages=" + average);
                    float myaverage = (float) average;
                    Major hi = new Major(0.0f, "hey");
                    hi.setRatingStar(myaverage);
//                    ratingStar.setRatingStar((float) average);

                    arrayList.add(new Major((float) average, "Iovine & Young Academy"));
                    arrayList.add(new Major(0, "Graphic Design"));
                    arrayList.add(new Major(0, "Computer Science"));
                    arrayList.add(new Major(0, "Business Administration"));
                    adapter.notifyDataSetChanged();//refresh


                }
                System.out.println("Averages=" + average);//for console verification of data
            }

        });



        arrayList = new ArrayList<>();



    }
}

